package com.demo.cn;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * date:2021/8/2
 * author:wsm(admin)
 * funcation:
 */
public class VideoMultyItem implements MultiItemEntity {
    private String url;
    //1 是视频  2  是商品
    private int flag;
    private String coverUrl;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public VideoMultyItem(String url, int flag, String coverUrl) {
        this.url = url;
        this.flag = flag;
        this.coverUrl = coverUrl;
    }

    @Override
    public int getItemType() {
        return flag;
    }
}

