package com.demo.cn;

import android.app.Application;
import android.content.Context;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * date:2021/4/29
 * author:wsm(admin)
 * funcation:
 */
public class MyApplication extends Application {

    private static MyApplication context;
    private WindowManager.LayoutParams windowParams = new WindowManager.LayoutParams();
    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
      
    }
    public WindowManager.LayoutParams getWindowParams() {
        return windowParams;
    }
    /**
     * @return 全局唯一的上下文
     * @describe: 获取全局Application的上下文
     */
    public static MyApplication getInstance() {
        return context;
    }
}
