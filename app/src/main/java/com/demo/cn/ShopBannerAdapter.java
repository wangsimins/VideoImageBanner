package com.demo.cn;

import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import cn.jzvd.JzvdStd;

/**
 * date:2021/7/28
 * author:wsm(admin)
 * funcation:
 */
public class ShopBannerAdapter extends BaseMultiItemQuickAdapter<VideoMultyItem, BaseViewHolder> {


    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public ShopBannerAdapter(List<VideoMultyItem> data) {
        super(data);
        addItemType(1, R.layout.banner_video);
        addItemType(2, R.layout.banner_image);

    }

    @Override
    protected void convert(BaseViewHolder helper, VideoMultyItem item) {
        switch (item.getItemType()) {
            case 1:
                //视频
                JzvdStd jzvdStd = helper.getView(R.id.player);
                //去掉
                jzvdStd.setUp(item.getUrl() + "", "", JzvdStd.SCREEN_NORMAL);
                Glide.with(mContext).load(item.getCoverUrl()).diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop().into(jzvdStd.posterImageView);//设置封面
                jzvdStd.setVideoImageDisplayType(jzvdStd.VIDEO_IMAGE_DISPLAY_TYPE_FILL_PARENT);//去掉黑框
                jzvdStd.startVideo();//自动播放

                break;
            case 2:
                ImageView imageView = helper.getView(R.id.image);
                Glide.with(mContext)
                        .load(item.getCoverUrl())
                        .centerCrop()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .into(imageView);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
                break;
            default:
                break;
        }
    }
}