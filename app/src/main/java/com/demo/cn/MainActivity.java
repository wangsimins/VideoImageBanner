package com.demo.cn;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.jzvd.Jzvd;
import cn.jzvd.JzvdStd;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.shop_banner)
    RecyclerView shopBanner;
    @BindView(R.id.banner_num)
    TextView bannerNum;
    private LinearLayoutManager mLayoutManager;
    private ShopBannerAdapter mShopBannerAdapter;
    private PagerSnapHelper mSnapHelper;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initBanner();
    }

    private void initBanner() {
        List<VideoMultyItem> mList = new ArrayList<>();
        //添加一个视频VideoMultyItem中三个参数，第一个参数是视频的地址，第二个参数1 是视频  2  是商品，第三个参数为图片地址（视频类型时用作视频封面）
        mList.add(new VideoMultyItem("https://f.video.weibocdn.com/001v6186gx07Ofj1jCJx01041200b8xc0E010.mp4?label=mp4_1080p&template=1920x1080.25.0&trans_finger=0bde055d9aa01b9f6bc04ccac8f0b471&media_id=4659228904980611&tp=8x8A3El:YTkl0eM8&us=0&ori=1&bf=4&ot=h&ps=3lckmu&uid=3ZoTIp&ab=3915-g1,5178-g0,966-g1,1493-g0,1192-g0,1191-g0,1258-g0&Expires=1627902317&ssig=RsS7BVBWq1&KID=unistore,video", 1, "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimages.669pic.com%2Felement_pic%2F55%2F44%2F38%2F7%2F07097c64a940a7c1b669345f1c6360ca.jpg&refer=http%3A%2F%2Fimages.669pic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1630490770&t=c8a62639eafd4fe4f26a545f28ab853d"));
        for (int i = 0; i < 5; i++) {
            mList.add(new VideoMultyItem(null, 2, "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimages.669pic.com%2Felement_pic%2F55%2F44%2F38%2F7%2F07097c64a940a7c1b669345f1c6360ca.jpg&refer=http%3A%2F%2Fimages.669pic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1630490770&t=c8a62639eafd4fe4f26a545f28ab853d"));
        }
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        shopBanner.setLayoutManager(mLayoutManager);
        mShopBannerAdapter = new ShopBannerAdapter(mList);
        mSnapHelper = new PagerSnapHelper();
        shopBanner.setOnFlingListener(null);
        mSnapHelper.attachToRecyclerView(shopBanner);
        shopBanner.setAdapter(mShopBannerAdapter);
        bannerNum.setText("1/" + mList.size());
        shopBanner.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager) shopBanner.getLayoutManager();
                int position = layoutManager.findFirstVisibleItemPosition();
                bannerNum.setText((position + 1) + "/" + mList.size());
            }
        });
        shopBanner.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        //停止滚动
                        autoPlay(recyclerView);
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        //拖动
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        //惯性滑动
                        Jzvd.releaseAllVideos();
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void autoPlay(RecyclerView recyclerView) {
        View view = mSnapHelper.findSnapView(mLayoutManager);

        if (view != null) {
            if (view instanceof RelativeLayout) {
                Jzvd.releaseAllVideos();
            } else {
                BaseViewHolder viewHolder = (BaseViewHolder) recyclerView.getChildViewHolder(view);

                if (viewHolder != null) {
                    JzvdStd myVideoPlayer = viewHolder.getView(R.id.player);
                    myVideoPlayer.startVideo();
                }
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        JzvdStd.releaseAllVideos();
    }
}
